package stockbit.hooks;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import stockbit.android_driver.AndroidDriverInstance;

import java.net.MalformedURLException;

import static stockbit.utils.Constans.ELEMENTS;
import static stockbit.utils.Utils.loadElementProperties;

public class AndroidDriverHooks {
    @Before(value = "@Android")
    public void initializedAndroidDriver() throws MalformedURLException {
        AndroidDriverInstance.initialize();
        loadElementProperties(ELEMENTS);
    }

    @After(value = "@Android")
    public void quitDriver(Scenario scenario) throws InterruptedException {
        boolean testStatus = scenario.isFailed();
        try {
            if (testStatus){
                final byte[] data = ((TakesScreenshot) AndroidDriverInstance.androidDriver).getScreenshotAs(OutputType.BYTES);
                scenario.attach(data, "image/png","Failed Screenshot");
            }
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            AndroidDriverInstance.quit();
        }
    }
}
