package stockbit.utils;

import io.github.cdimascio.dotenv.Dotenv;
import net.bytebuddy.asm.Advice;

import javax.lang.model.util.Elements;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Objects;
import java.util.Properties;

public class Utils {
    public static Properties ELEMENTS;

    public static void loadElementProperties(String directory) {
        File folder = new File(directory);
        File[] listOfFiles = folder.listFiles();
        ELEMENTS = new Properties();

        for (int i = 0; i < Objects.requireNonNull(listOfFiles).length; i++) {
            if (listOfFiles[i].isFile() && listOfFiles[i].toString().contains(".properties")) {
                try {
                    System.out.println(listOfFiles[i].getName());
                    ELEMENTS.load(new FileInputStream(directory + listOfFiles[i].getName()));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static String env(String key) {
        Dotenv dotenv = Dotenv.load();
        return dotenv.get(key);
    }
}