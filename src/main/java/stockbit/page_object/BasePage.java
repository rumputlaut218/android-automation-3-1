package stockbit.page_object;

import io.appium.java_client.AppiumBy;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import stockbit.android_driver.AndroidDriverInstance;
import stockbit.utils.Utils;

import static org.openqa.selenium.remote.ErrorCodes.TIMEOUT;

public class BasePage {
    public static By element(String elementLocator) {
        String elementValue = Utils.ELEMENTS.getProperty(elementLocator);
        String[] locator = elementValue.split("_");
        String locatorType = locator[0];
        String locatorValue = elementValue.substring(elementValue.indexOf("_") + 1);
        switch (locatorType){
            case "id" -> {
                return By.id(locatorValue);
            }
            case "xpath" -> {
                return By.xpath(locatorValue);
            }
            case "containsText" -> {
                return By.xpath(String.format("//*[contains(@text, '%s')]", locatorValue));
            }
            case "cssSelector" -> {
                return By.cssSelector(locatorValue);
            }
            case "accessibilityId" -> {
                return AppiumBy.accessibilityId(locatorValue);
            }
            default -> throw new IllegalStateException("Unexpected Value: " + locatorType);
        }
    }
    public static AndroidDriver driver(){
        return AndroidDriverInstance.androidDriver;
    }
    public static <T> T waitUntil(ExpectedCondition<T> expectation) {
        WebDriverWait wait = new WebDriverWait(driver(), (TIMEOUT));
        return wait.until(expectation);
    }
    public static void tap(String element){
        waitUntil(ExpectedConditions.elementToBeClickable(element(element))).click();
    }
    public static void typeOn(By element, String text) {
        waitUntil(ExpectedConditions.visibilityOfElementLocated(element)).sendKeys(text);
    }
    public void typeOn(String element, String text){
        driver().findElement(element(element)).sendKeys(text);
    }
    public static void assertIsDisplay(By element) {
        try {
            driver().findElement(element).isDisplayed();
        }catch (NoSuchElementException e){
            throw new AssertionError(String.format("This element '%s' not found",element));
        }
    }
}